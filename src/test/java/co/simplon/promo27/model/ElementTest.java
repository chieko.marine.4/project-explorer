package co.simplon.promo27.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementTest {
    Path tempFolder;

    @BeforeEach
    public void setUp() {
        TempFolder.generate();
        tempFolder = TempFolder.temp;

    }

    @AfterEach
    public void reset() {
        TempFolder.cleanUp();
    }

    @Test
    void shouldNotBeDirectory() {
        Element file = new Element(tempFolder.resolve("testfile").toFile());
        assertFalse(file.isDirectory());
    }

    @Test
    void shouldBeDirectory() {
        Element folder = new Element(tempFolder.resolve("testfolder").toFile());
        assertTrue(folder.isDirectory()); // TODO: Tester avec un dossier
    }

    @Test
    void shouldRemoveFile() throws IOException {
        Element RemoveFile = new Element(tempFolder.resolve("testfile").toFile());
        RemoveFile.remove();// TODO: Tester la suppression d'un fichier
        assertFalse(false);tempFolder.resolve("testfile").toFile().exists();
    }

    @Test
    void shouldRenameFile() throws IOException{
        Element RenameFile = new Element(tempFolder.resolve("testfile").toFile());
       RenameFile.rename("test1"); //TODO: Tester renommer le fichier
       assertFalse(false);tempFolder.resolve("testfile").toFile().exists();
       assertTrue(true);tempFolder.resolve("test1").toFile().exists();
    }
    


    @Test
    void shouldNotRenameIfNameExists() throws IOException {
        Element RenameIfNameExists = new Element(tempFolder.resolve("testfile").toFile());
        RenameIfNameExists.rename("test1"); // TODO: Tester de renommer avec un nom de fichier déjà existant
    }
}
