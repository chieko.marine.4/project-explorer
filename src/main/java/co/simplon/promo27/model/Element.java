package co.simplon.promo27.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Classe représentant un fichier/dossier permettant de manipuler celui ci (suppression, renommer).
 * Surtout un wrapper autour de la classe File et du Files de Java
 */
public class Element {
    private boolean directory;
    private String name;
    private File file;
    

    public Element(File file) {
        
        this.name = file.getName();
        this.file = file;
        this.directory = this.file.isDirectory();

    }

    /**
     * L'élément est-il un dossier
     */
    public boolean isDirectory() {
        return directory;
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        return file;
    }

    /**
     * Supprime l'élément ciblé
     * @throws IOException Exception si l'élément n'existe pas ou que le programme n'a pas les droits pour supprimer
     */
    public void remove() throws IOException {
        Files.delete(file.toPath());
    }

    /**
     * Renome l'élément et met à jour les propriétés correspondantes de l'instance
     * @param newName Le nouveau nom de l'élément
     * @throws IOException Exception si un élément avec le même nom existe ou que le programme n'a pas les droits
     */
    public void rename(String newName) throws IOException {
        Files.move(file.toPath(), file.toPath().getParent().resolve(newName));
        name = newName;
        file = file.toPath().getParent().resolve(newName).toFile();
    }


    

}
