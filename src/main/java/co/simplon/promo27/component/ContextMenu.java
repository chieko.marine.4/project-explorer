package co.simplon.promo27.component;

import java.io.IOException;
import java.util.Optional;

import javax.swing.text.AbstractDocument.Content;

import co.simplon.promo27.App;
import co.simplon.promo27.model.Element;
import co.simplon.promo27.model.Explorer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

/**
 * Composant d'interface externalisé dans sa propre classe afin de ne pas avoir toutes les méthodes dans le PrimaryController
 * Représente un menu contextuel avec des actions possibles sur un fichier/dossier
 */
public class ContextMenu extends HBox {
    private Explorer explorer = Explorer.getInstance();
    /**
     * L'élément sélectionné dans l'interface auquel s'appliquera les différentes actions du menu
     */
    private Element element;
    /**
     * Permet de déclencher un événement qui pourra être intercepter par le parent (ici le PrimaryController).
     * Utile pour indiquer qu'une action a eu lieue et que l'affichage du parent doit être mis à jour
     */
    private ObjectProperty<EventHandler<Event>> propertyOnAction = new SimpleObjectProperty<EventHandler<Event>>();

    public ContextMenu() {
        //La vue se trouve avec les autres dans src/main/resources/co/simplon/promo27
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("component/contextmenu.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Ouvre une popup pour renommer l'élément sélectionné
     */
    @FXML
    public void renameAction(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Rename");
        dialog.setHeaderText("Rename " + element.getName());
        dialog.setContentText("Please enter new name:");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(newName -> {
            try {
                element.rename(newName);
                propertyOnAction.get().handle(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    
    /**
     * "Copie" l'élément sélectionné dans l'explorer
     */
    @FXML
    public void copyAction(ActionEvent event) {
        explorer.setCopied(element);
        propertyOnAction.get().handle(event);
    }

    @FXML
    public void removeAction(ActionEvent event) {

        System.out.println("click");

        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning ");
        alert.setHeaderText("Are you sur to remove this folder?");
        alert.setContentText("Are you sur to remove this folder?");

        alert.showAndWait();

        if(element.isDirectory()){
            System.out.println(element.getFile().length());
            
            alert.setTitle("Information ");
            alert.setHeaderText("Look, an Information Dialog");
            alert.setContentText("I have a great message for you!");

            alert.showAndWait();
        }

        try {
            element.remove();
            //updateDisplay();
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        propertyOnAction.get().handle(event);
    }



        @Override
        public void handle(KeyEvent event) {
            if (!event.getCharacter().matches("[1-9]")) {
                event.consume();
            }
        }        @Override
        public void handle(KeyEvent event) {
            if (!event.getCharacter().matches("[1-9]")) {
                event.consume();
            }
        }
    }
    
    ///Clipboard clipboard = Clipboard.getSystemClipboard();
    //ClipboardContent content = new ClipboardContent();
    
 




//////////////////////
    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
        this.setVisible(element != null);
    }

    //On peut ignorer tout ça, c'est juste pour l'event
    public final ObjectProperty<EventHandler<Event>> onActionProperty() {
        return propertyOnAction;
    }

    public final void setOnAction(EventHandler<Event> handler) {
        propertyOnAction.set(handler);
    }

    public final EventHandler<Event> getOnAction() {
        return propertyOnAction.get();

    } 


    

    
}